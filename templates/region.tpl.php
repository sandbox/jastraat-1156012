<?php

/**
 * @file
 * Default theme implementation to display a region.
 */
?>
<?php if ($content): ?>
  <section class="<?php print $classes; ?>">
    <?php print $content; ?>
  </section>
<?php endif; ?>
