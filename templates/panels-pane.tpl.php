<?php
/**
 * @file panels-pane.tpl.php
 * Main panel pane template
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>
<?php $tag = $title ? 'section' : 'div'; ?>
<<?php print $tag; ?> <?php print $id; ?> class="<?php print $classes; ?>">
  <div class="panel-inner pane-inner">
  
    <?php if ($admin_links): ?>
      <div class="admin-links panel-hide">
        <?php print $admin_links; ?>
      </div>
    <?php endif; ?>
  
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <?php if ($pane->type == 'node_content'): ?>
        <h1 class="pane-title"><?php print $title; ?></h1>
      <?php else: ?> 
         <h2 class="pane-title"><?php print $title; ?></h2>
      <?php endif; ?>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
  
    <?php if ($feeds): ?>
      <div class="feed">
        <?php print $feeds; ?>
      </div>
    <?php endif; ?>
  
    <div class="pane-content">
      <?php if ($pane->type == 'node_title'): ?>
        <h1 class="node-title"><?php print render($content); ?></h1>
     <?php else: ?> 
        <?php print render($content); ?>
      <?php endif; ?>
    </div>
  
    <?php if ($links): ?>
      <div class="links">
        <?php print $links; ?>
      </div>
    <?php endif; ?>
  
    <?php if ($more): ?>
      <div class="more-link">
        <?php print $more; ?>
      </div>
    <?php endif; ?>
  
  </div>
</<?php print $tag; ?>>