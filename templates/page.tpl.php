<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 */
?>

<div id="page-wrapper" class="<?php print $classes; ?>">
  <div id="page">
    <header id="header" role="banner">
      <div class="pagewidth clearfix">
        <?php if ($secondary_menu): ?>
          <nav id="secondary-menu" role="navigation"><?php print $secondary_menu; ?></nav>
        <?php endif; ?>
        <?php print render($page['header']); ?>
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

        <?php if ($site_name || $site_slogan): ?>
          <hgroup id="name-and-slogan">
            <?php if ($site_name): ?>
               <?php if ($title): ?>
                 <div id="site-name">
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                 </div>
               <?php else: /* Use h1 when the content title is empty */ ?>
                  <h1 id="site-name">
                     <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                  </h1>
               <?php endif; ?>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <div id="site-slogan"><?php print $site_slogan; ?></div>
            <?php endif; ?>
          </hgroup> <!-- /#name-and-slogan -->
        <?php endif; ?>
        
        <?php if ($page['menu_bar'] || $main_menu): ?>
          <nav id="navbar" role="navigation">
            <?php if ($page['menu_bar']){ ?>
              <?php print render($page['menu_bar']); ?>
            <?php } else { ?>
              <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix')), 'heading' => array('text' => t('Main menu'),'level' => 'h2','class' => array('element-invisible')),)); ?>
            <?php } ?>
          </nav> <!-- /nav -->
        <?php endif; ?>
      </div>
    </header> <!-- /header -->

    <div id="main" role="main">
      <div class="pagewidth clearfix">
    
        <?php if (!empty($messages) || !empty($help)): ?>
          <div id="messages">
            <?php if (!empty($messages)) {print $messages;} ?>
            <?php if (!empty($help)) {print $help;} ?>
          </div> <!-- /#messages -->
        <?php endif; ?>
        
        <div id="content" class="clearfix">
           <?php print render($title_prefix); ?>
           <?php if ($title): ?>
             <h1 class="title" id="page-title">
                <?php print $title; ?>
             </h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php if ($tabs = render($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if ($action_links = render($action_links)): ?><ul class="action-links"><?php print $action_links; ?></ul><?php endif; ?>
          <a id="main-content" class="element-invisible element-focusable"></a>
          <?php print render($page['content']); ?>
        </div>
      </div>
    </div> <!-- /#main -->
    <?php if ($page['footer']): ?>
      <footer id="footer" role="contentinfo">
        <div class="pagewidth clearfix">
          <?php print render($page['footer']); ?>
        </div>
      </footer>
   <?php endif; ?>
  </div>
</div> <!-- /#page-wrapper -->
