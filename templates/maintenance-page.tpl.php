<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
  </head>

  <body class="<?php print $classes; ?>" <?php print $attributes;?>>
    <div id="skip-link">
      <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
    </div>
    <?php print $page_top; ?>
    <div id="page-wrapper"><div id="page">
      <header id="header" role="banner">
        <div class="pagewidth clearfix">
          <?php if (!empty($logo)): ?>
            <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php endif; ?>

          <?php if ($site_name || $site_slogan): ?>
            <hgroup id="name-and-slogan">
              <?php if ($site_name): ?>
                <h1 id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                </h1>
              <?php endif; ?>
  
              <?php if ($site_slogan): ?>
                <div id="site-slogan"><?php print $site_slogan; ?></div>
              <?php endif; ?>
            </hgroup> <!-- /#name-and-slogan -->
          <?php endif; ?>
        </div>
      </header> <!-- /header -->

      <div id="main" role="main">
        <div class="pagewidth clearfix">
          <?php if (!empty($messages)): print $messages; endif; ?>
          <div id="content" class="clearfix">
            <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
            <a id="main-content"></a>
            <?php print $content; ?>
          </div> <!-- /content-content -->
        </div> <!-- /content -->
      </div><!-- /main -->

      <footer id="footer" role="contentinfo">
        <div class="pagewidth clearfix">
          <?php if (!empty($footer)): print $footer; endif; ?>
        </div>
      </footer>

    </div></div> <!-- /page -->
    <?php print $scripts; ?>
    <?php print $page_bottom; ?>
  </body>
</html>
