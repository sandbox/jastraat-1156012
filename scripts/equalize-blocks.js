(function ($) {
  Drupal.behaviors.innovate_equalizeblocks = {
    attach: function(context) {
      // check if there are images that still need to load
      if(!($('.panel-row-resize img').length > 0) || $('.panel-row-resize img').width() > 0 || $('.panel-row-resize img').complete) {
        if ($(window).width() > 747) {						 
          this.adjustHeights(context);	
          //alert('resized early');
        }
      } 
      //If images still need to load (webkit), wait to resize
      else {
        this.resizeLater = true;
      }
      var _ = this;
      $(window).resize(function(){
        $('.panel-content-color').css('height','auto');
        $('.panel-content-color .rounded-corners').css('height','auto');
        if ($(window).width() > 747) {
          _.adjustHeights(context);
          $('#header #navbar .content').show();
        }
      });
    },

    resizeLater: false,

    adjustHeights: function (context) {
      var resize_rows = $('.panel-row-resize', context);
      var adjusted = $([]);
      for (var i = 0; i < resize_rows.length; i++) {
        // recursively resize children
        if($('.panel-row-resize', resize_rows[i]).length > 0) {
          var children = Drupal.behaviors.innovate_equalizeblocks.adjustHeights(resize_rows[i]);
          adjusted = adjusted.add(children);
        }
        var max_height = 0;
        // don't run height adjustments twice on a column
        var resize_columns = $('.panel-content-color', resize_rows.eq(i)).filter(function(k){
          if(adjusted.filter(this).length > 0) return false; return true;
        });
        for (j = 0; j < resize_columns.length; j++) {
          if ( $(resize_columns[j]).height() > max_height ) {
            max_height = $(resize_columns[j]).height();
          }
        }
        $(resize_columns).css('height', max_height + 'px');
        if ($('.rounded-corners', resize_columns).length > 0) {
          $('.rounded-corners ', resize_columns).css('height', max_height + 'px');
        }
        adjusted = adjusted.add(resize_columns);
        // if a column was just stretched, there may be empty space below the
        // last child column, so stretch those to fill the parent
        resize_columns.each(function(l) {
          if($('.panel-row-resize', this).length > 0) {
            var totalHeight = 0;
            $(this).children().each(function(){
              totalHeight = totalHeight + $(this).outerHeight();
            });
            var deltaHeight = max_height - totalHeight;
            if(deltaHeight > 0) {
              $('.panel-row-resize:last .panel-content-color', this).each(function(){
                $(this).css('height', deltaHeight + $(this).outerHeight() + 'px');
              });
            }
          }
        });
      }
      return adjusted;
    }
  };

  //Because webkit doesn't load image sizes before this executes every time, call later
  $(window).load(function() {
    if (Drupal.behaviors.innovate_equalizeblocks.resizeLater && $(window).width() > 747) {
      Drupal.behaviors.innovate_equalizeblocks.adjustHeights(document);	
      //alert('resized later');
    }
  });

})(jQuery);
