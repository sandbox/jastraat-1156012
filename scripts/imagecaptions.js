(function ($) {
  Drupal.behaviors.innovate_imagecaptions = {
    attach: function(context) {
       //Add only on page load
       if(context == document){
         $('.page-node .field-type-text-with-summary img').each(function() {
            if ($(this).attr('alt')) {
              var floatClasses = '';
              var maxWidth = '';
              if ($(this).css('float')) {
                floatClasses = ' ' + $(this).css('float');
                $(this).css('float', 'none');
                $(this).css('margin', '0');
              }
              if($(this).width() > 0) {
                maxWidth = ' style="max-width:' + $(this).width() + 'px;"';
              }
              $(this).wrap('<div class="inline-image-wrapper' + floatClasses  + '"></div>');
              $(this).parent().append('<div class="inline-image-caption"' + maxWidth + '>' + $(this).attr('alt')  + '</div>');
            }
         });
      }
    }
  };
})(jQuery);
