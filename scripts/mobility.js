(function ($) {
  Drupal.behaviors.innovate_mobility = {
    attach: function(context) {
       //Inline images need to resize on browser resize
       $('.page-node .field-type-text-with-summary img').each(function() {
          $(this).removeAttr('height');
          $(this).removeAttr('width');
          $(this).css('width', 'auto');
          $(this).css('height', 'auto');
       });
      $('.node-teaser .field-type-text-with-summary img').each(function() {
          $(this).removeAttr('height');
          $(this).removeAttr('width');
          $(this).css('width', 'auto');
          $(this).css('height', 'auto');
          $(this).css('margin-left', '');
          $(this).css('margin-right', '');
       });

      //Inline tables need to resize if they can
      $('.page-node .field-type-text-with-summary table td').each(function() {
          $(this).removeAttr('width');
          $(this).css('width', 'auto');
       });

      //Allow for hiding and showing the main menu at mobile widths
      $('#header #navbar h2').click(function() {
        $('#header #navbar .content').toggle();
		    $('#header #navbar .secondary-main-menu-links').toggle();
        if ($('#header #navbar h2').hasClass('open-menu')) {
          $('#header #navbar h2').removeClass('open-menu');
        }
        else {
          $('#header #navbar h2').addClass('open-menu');
        }
      }); 
      $(window).resize(function(){
         if ($(window).width() > 767) {
           $('#header #navbar h2').removeClass('open-menu');
         }
      });
	    //Move secondary menu links into #navbar
	    if ($(window).width() < 768) {
	      $('.secondary-main-menu-links').insertAfter($('#navbar .region-menu-bar'));
	    }

      //Search placeholder text fallback for IE and Firefox
      if (! ("placeholder" in document.createElement("input"))) {
        $(':input[placeholder]').each(function(){			   
          if( $(this).val() == $(this).attr('placeholder') ) {
            $(this).val('');
          }
          if(!$(this).val() > 0){
            $(this).val($(this).attr('placeholder'));
            $(this).addClass('input-placeholder');
          }
       }).live('focus', function(e){
         if($(this).hasClass('input-placeholder')){
           $(this).val('');
           $(this).removeClass('input-placeholder');
         }
      }).live('blur', function(e){
        if(!$(this).val()){
          $(this).addClass('input-placeholder');
          $(this).val($(this).attr('placeholder'));
		  
        }
      });
     }
    }
  };
})(jQuery);