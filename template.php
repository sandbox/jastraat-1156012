<?php

/**
 * Implements hook_page_alter()
 * Add meta elements to the head
 */
function innovate_page_alter($page) { 
  if (theme_get_setting('innovate_meta_description') && drupal_is_front_page()) {	
    $meta_description = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'description',
        'content' =>  ''. variable_get('site_slogan')
      )
    ); 
    drupal_add_html_head($meta_description, 'meta_description');
  }
  
  if (theme_get_setting('innovate_include_chrome_frame')) {
    $meta_chromeframe = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'http-equiv' => 'X-UA-Compatible',
        'content' =>  'IE=Edge,chrome=1'
      )
    ); 
    drupal_add_html_head($meta_chromeframe, 'meta_chromeframe');
  }
  
  if (theme_get_setting('innovate_use_mobile_viewport')) {
    $meta_viewport = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'viewport',
        'content' =>  theme_get_setting('innovate_mobile_viewport')
      )
    ); 
    drupal_add_html_head($meta_viewport, 'meta_viewport');
  }
  //Since JS is added to the footer by default, make sure to include this file at the top for IE HTML5 support
  $html5_js = array(
    '#type' => 'markup',
    '#markup' => innovate_conditional_js('lt IE 9', drupal_get_path('theme', 'innovate') . '/scripts/html5.js')
  ); 
  drupal_add_html_head($html5_js, 'html5_js');
}

/**
 * Implements hook_html_head_alter().
 * We are overwriting the default meta character type tag with HTML5 version.
 */
function innovate_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}

/**
 * Changes appearance of default search block.
 */
function innovate_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    if (module_exists('elements')) {
      $form['search_block_form']['#type'] = 'searchfield';
    }
    $form['search_block_form']['#attributes']['placeholder'] = t('Site Search');
  }
  if ($form_id == 'user_login') {
    $form['name']['#attributes']['autocorrect'] = 'off';
    $form['name']['#attributes']['autocapitalization'] = 'off';
    $form['name']['#attributes']['autofocus'] = 'autofocus';
  }
}

/**
 * Changes the search form to use the "search" input element of HTML5 if Elements module not installed.
 */
function innovate_preprocess_search_block_form(&$variables) {
  if (!module_exists('elements')) {
    $variables['search_form'] = str_replace('type="text"', 'type="search"', $variables['search_form']);
  }
}

/**
* Implements hook_preprocess_html().
*/
function innovate_preprocess_html(&$variables) {
  if (module_exists('rdf')) {
    $variables['rdf_version'] = ' version="HTML+RDFa 1.1"';
  } 
  else {
    $variables['rdf_version'] = '';
  }	

  // Make a list of base themes and the current theme.
  $themes = $GLOBALS['base_theme_info'];
  $themes[] = $GLOBALS['theme_info'];
  foreach (array_keys($themes) as $key) {
    $theme_path = dirname($themes[$key]->filename) . '/';
    if (isset($themes[$key]->info['stylesheets-conditional'])) {
      foreach (array_keys($themes[$key]->info['stylesheets-conditional']) as $condition) {
        foreach (array_keys($themes[$key]->info['stylesheets-conditional'][$condition]) as $media) {
          foreach ($themes[$key]->info['stylesheets-conditional'][$condition][$media] as $stylesheet) {
            // Add each conditional stylesheet.
            drupal_add_css(
              $theme_path . $stylesheet,
              array(
                'group' => CSS_THEME,
                'browsers' => array(
                  'IE' => $condition,
                  '!IE' => FALSE,
                ),
                'every_page' => TRUE,
              )
            );
          }
        }
      }
    }
  }
  
  if (theme_get_setting('innovate_use_fixed_layout')) {
    $variables['classes_array'][] = 'fixed-width-layout';
  }

  if(theme_get_setting('innovate_no_pane_margins')) {
    $variables['classes_array'][] = 'no-pane-margins';
  }

  $node = menu_get_object();

  if ($node) {
    if (variable_get('node_submitted_' . $node->type, TRUE)) {
      $variables['classes_array'][] = 'node-display-submitted';
    }
  }

  if (module_exists('page_manager') && page_manager_get_current_page()) {
    $variables['classes_array'][] = 'page-panels';
    if ($node && !drupal_get_title()) {
      $head_title = array(
        'title' => strip_tags($node->title), 
        'name' => check_plain(variable_get('site_name', 'Drupal')),
      );
      $variables['head_title'] = implode(' | ', $head_title);
    }
  }
  else if (module_exists('page_manager')) {
    $variables['classes_array'][] = 'page-without-panels';
  }

  //Add JS to add captions to inline images
  if (theme_get_setting('innovate_imagecaptions')) {
    drupal_add_js(drupal_get_path('theme', 'innovate') . '/scripts/imagecaptions.js', array('group' => JS_THEME, 'every_page' => TRUE));
  }  

  //Add JS to set panel heights within a row
  if (theme_get_setting('innovate_equalizejs')) {
    drupal_add_js(drupal_get_path('theme', 'innovate') . '/scripts/equalize-blocks.js', array('group' => JS_THEME, 'every_page' => TRUE));
  }
}

/**
* Implements hook_preprocess_page().
*/
function innovate_preprocess_page(&$variables) { 
  $menu_title = 'Quick Links';
  $secondary = variable_get('menu_secondary_links_source');
  if ($secondary) {
    $menus = menu_get_menus(TRUE);
    if ($menus[$secondary]) {
      $menu_title = $menus[$secondary];
    }
  }
  
  //Add quick links menu (secondary menu)
  if (theme_get_setting('toggle_secondary_menu') && module_exists('ctools') && !empty($variables['secondary_menu'])) {
    ctools_include('jump-menu');
    $links = array();
    foreach ($variables['secondary_menu'] as $link) {
      $links[$link['href']] = $link['title'];
    }
    $variables['secondary_menu'] = drupal_render(ctools_jump_menu(array(), $form_state, $links, array('choose' => $menu_title)));
  }
  //If no ctools, treat secondary links as a regular menu
  else if (theme_get_setting('toggle_secondary_menu') && !empty($variables['secondary_menu'])) {
    $variables['secondary_menu'] = theme('links__system_secondary_menu', array(
      'links' => $variables['secondary_menu'],
      'attributes' => array(
        'id' => 'secondary-menu-links',
        'class' => array('links', 'inline', 'clearfix'),
      ),
      'heading' => array(
        'text' => t($menu_title),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
}

/**
* Implements template_preprocess_comment()
*/
function innovate_preprocess_comment(&$variables) {
  $comment = $variables['elements']['#comment'];
  $uri = entity_uri('comment', $comment);
  $uri['options'] += array('attributes' => array(
      'class' => 'permalink',
      'rel' => 'bookmark',
      'property' => 'dc:date dc:created',
      'datatype' => 'xsd:dateTime',
      'content' => $comment->created
    ));
  $created = format_date($comment->created);
  $variables['permalink'] = l($created, $uri['path'], $uri['options']);
}

/**
* Implements hook_preprocess_maintenance_page().
*/
function innovate_preprocess_maintenance_page(&$variables) {
  innovate_preprocess_html($variables);
  innovate_preprocess_page($variables);
  //Since JS is added to the footer by default, make sure to include this file at the top for IE HTML5 support
  $html5_js = array(
    '#type' => 'markup',
    '#markup' => innovate_conditional_js('lt IE 9', drupal_get_path('theme', 'innovate') . '/scripts/html5.js')
  ); 
  drupal_add_html_head($html5_js, 'html5_js');
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $variables
 *   - title: An optional string to be used as a navigational heading to give
 *     context for breadcrumb links to screen-reader users.
 *   - title_attributes_array: Array of HTML attributes for the title. It is
 *     flattened into a string within the theme function.
 *   - breadcrumb: An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function innovate_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('innovate_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('innovate_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('innovate_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('innovate_breadcrumb_title')) {
        $item = menu_get_item();
        if (!empty($item['tab_parent'])) {
          // If we are on a non-default tab, use the tab's title.
          $title = check_plain($item['title']);
        }
        else {
          $title = drupal_get_title();
        }
        if ($title) {
          $trailing_separator = $breadcrumb_separator;
        }
      }
      elseif (theme_get_setting('innovate_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }

      // Provide a navigational heading to give context for breadcrumb links to
      // screen-reader users.
      if (empty($variables['title'])) {
        $variables['title'] = t('You are here');
      }
      // Unless overridden by a preprocess function, make the heading invisible.
      if (!isset($variables['title_attributes_array']['class'])) {
        $variables['title_attributes_array']['class'][] = 'element-invisible';
      }
      $heading = '<h2' . drupal_attributes($variables['title_attributes_array']) . '>' . $variables['title'] . '</h2>';

      return '<div class="breadcrumb">' . $heading . implode($breadcrumb_separator, $breadcrumb) . $trailing_separator . $title . '</div>';
    }
  }
  // Otherwise, return an empty string.
  return '';
}

// Simple theme function to output js in a conditional comment.
function innovate_conditional_js($condition, $file_path) {
  $output = '<!--[if ' . $condition . ']><script type="text/javascript" src="' . base_path() . $file_path . '?' . variable_get('css_js_query_string') . '"></script><![endif]-->';
  return $output;
}

/**
 * Set a class on the iframe body element for WYSIWYG editors. This allows you
 * to easily override the background for the iframe body element.
 * This only works for the WYSIWYG module: http://drupal.org/project/wysiwyg
 */
function innovate_wysiwyg_editor_settings_alter(&$settings, &$context) {
  $settings['bodyClass'] = 'wysiwygeditor';
}

/**
 * Add a class to the submenu in the sidebar
 * This can be used for collapsing on mobile.
 */
function innovate_preprocess_panels_pane(&$variables) { 
  if (isset($variables['pane']) && $variables['pane']->type == 'block' && ($variables['pane']->panel == 'rightSb' || $variables['pane']->panel == 'leftSb') && strpos($variables['pane']->subtype, 'menu_block') !== FALSE && empty($variables['pane']->css['css_id'])) {
	$variables['classes_array'][] = 'secondary-main-menu-links';
	$variables['classes_array'][] = 'clearfix';
  }
}