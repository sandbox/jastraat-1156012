<?php

/**
 * @file
 * Definition of the 'withheading' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('With block heading'),
  'description' => t('Display region with a defined block-style heading.'),
  'render region' => 'innovate_withheading_style_render_region',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_innovate_withheading_style_render_region($vars) {
  $output = '';
  $output .= '<div class="pane-with-heading">';
  $output .= implode('<div class="panel-separator"></div>', $vars['panes']);
  $output .= '</div>';
  return $output;
}

