<?php

/**
 * @file
 * Definition of the 'nopadding' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('No padding - for feature images'),
  'description' => t('Display region without padding for content such as feature images.'),
  'render region' => 'innovate_nopadding_style_render_region',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_innovate_nopadding_style_render_region($vars) {
  $output = '';
  $output .= '<div class="pane-no-padding">';
  $output .= implode('<div class="panel-separator"></div>', $vars['panes']);
  $output .= '</div>';
  return $output;
}

