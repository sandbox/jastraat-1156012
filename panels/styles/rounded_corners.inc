<?php

/**
 * @file
 * Definition of the innovate 'rounded corners' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Rounded corners'),
  'description' => t('Display region with rounded corners.'),
  'render region' => 'innovate_rounded_corners_style_render_region',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_innovate_rounded_corners_style_render_region($vars) {
  $output = '';
  $output .= '<div class="rounded-corners">';
  $output .= implode('<div class="panel-separator"></div>', $vars['panes']);
  $output .= '</div>';
  return $output;
}

