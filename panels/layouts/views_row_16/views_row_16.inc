<?php

$plugin = array(
  'title' => t('Views row: 16'),
  'category' => t('Views Layouts'),
  'icon' => 'icon.png',
  'css' => '../../../lib/grid.css',
  'theme' => 'views_row_16',
  'regions' => array(
    'sixcol-left' => t('Six Column Left'),
    'tencol-right' => t('Ten Column Right'),
    'tencol-left' => t('Ten Column Left'),
    'sixcol-right' => t('Six Column Right'),
    'eightcol-left' => t('Eight Column Left'),
    'eightcol-right' => t('Eight Column Right'),
    'elevencol-left' => t('Eleven Column Left'),
    'fivecol-right' => t('Five Column Right'),
    'fivecol-left' => t('Five Column Left'),
    'elevencol-right' => t('Eleven Column Right'),
    'twelvecol-left' => t('Twelve Column Left'),
    'fourcol-right' => t('Four Column Right'),
    'fourcol-left' => t('Four Column Left'),
    'twelvecol-right' => t('Twelve Column Right'),
  ),
);