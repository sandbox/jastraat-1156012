<div class="panel-display views_row_16 container_16" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if (!empty($content['sixcol-left']) || !empty($content['tencol-right'])): ?>
  <div class="panel-row panel-row-6-10 clearfix">
    <div class="panel-sixcol-left panel-panel panel-col panel-col-first grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-left']; ?>
      </div>
    </div>
    <div class="panel-tencol-right panel-panel panel-col panel-col-last grid_10">
      <div class="panel-content-color">
        <?php print $content['tencol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  
  <?php if (!empty($content['tencol-left']) || !empty($content['sixcol-right'])): ?>
  <div class="panel-row panel-row-10-6 clearfix">
    <div class="panel-tencol-left panel-panel panel-col panel-col-first grid_10">
      <div class="panel-content-color">
        <?php print $content['tencol-left']; ?>
      </div>
    </div>
    <div class="panel-sixcol-right panel-panel panel-col panel-col-last grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  
  <?php if (!empty($content['eightcol-left']) || !empty($content['eightcol-right'])): ?>
  <div class="panel-row panel-row-8-8 clearfix">
    <div class="panel-eightcol-left panel-panel panel-col panel-col-first grid_8">
      <div class="panel-content-color">
        <?php print $content['eightcol-left']; ?>
      </div>
    </div>
    <div class="panel-eightcol-right panel-panel panel-col panel-col-last grid_8">
      <div class="panel-content-color">
        <?php print $content['eightcol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  
  <?php if (!empty($content['elevencol-left']) || !empty($content['fivecol-right'])): ?>
  <div class="panel-row panel-row-11-5 clearfix">
    <div class="panel-elevencol-left panel-panel panel-col panel-col-first grid_11">
      <div class="panel-content-color">
        <?php print $content['elevencol-left']; ?>
      </div>
    </div>
    <div class="panel-fivecol-right panel-panel panel-col panel-col-last grid_5">
      <div class="panel-content-color">
        <?php print $content['fivecol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
    
  <?php if (!empty($content['fivecol-left']) || !empty($content['elevencol-right'])): ?>
  <div class="panel-row panel-row-5-11 clearfix">
    <div class="panel-fivecol-left panel-panel panel-col panel-col-first grid_5">
      <div class="panel-content-color">
        <?php print $content['fivecol-left']; ?>
      </div>
    </div>
    <div class="panel-elevencol-right panel-panel panel-col panel-col-last grid_11">
      <div class="panel-content-color">
        <?php print $content['elevencol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
    
  <?php if (!empty($content['twelvecol-left']) || !empty($content['fourcol-right'])): ?>
  <div class="panel-row panel-row-12-4 clearfix">
    <div class="panel-twelvecol-left panel-panel panel-col panel-col-first grid_12">
      <div class="panel-content-color">
        <?php print $content['twelvecol-left']; ?>
      </div>
    </div>
    <div class="panel-fourcol-right panel-panel panel-col panel-col-last grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
      
  <?php if (!empty($content['fourcol-left']) || !empty($content['twelvecol-right'])): ?>
  <div class="panel-row panel-row-4-12 clearfix">
    <div class="panel-fourcol-left panel-panel panel-col panel-col-first grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-left']; ?>
      </div>
    </div>
    <div class="panel-twelvecol-right panel-panel panel-col panel-col-last grid_12">
      <div class="panel-content-color">
        <?php print $content['twelvecol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
</div>
