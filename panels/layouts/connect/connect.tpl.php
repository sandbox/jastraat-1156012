<div class="panel-display connect container_16" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <?php if (!empty($content['full-top'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-top panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-top']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['tencol-left']) || !empty($content['sixcol-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-10-6 clearfix">
    <div class="panel-tencol-left panel-panel panel-col panel-col-first grid_10">
      <div class="panel-content-color">
        <?php print $content['tencol-left']; ?>
      </div>
    </div>
    <div class="panel-sixcol-right panel-panel panel-col panel-col-last grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['full-middle'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-middle panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-middle']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['fourcol-left']) || !empty($content['fourcol-leftcenter']) || !empty($content['fourcol-rightcenter']) || !empty($content['fourcol-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-4-4-4-4 clearfix">
    <div class="panel-fourcol-left panel-panel panel-col panel-col-first grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-left']; ?>
      </div>
    </div>
    <div class="panel-fourcol-leftcenter panel-panel panel-col grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-leftcenter']; ?>
      </div>
    </div>
    <div class="panel-fourcol-rightcenter panel-panel panel-col grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-rightcenter']; ?>
      </div>
    </div>
    <div class="panel-fourcol-right panel-panel panel-col panel-col-last grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['full-bottom'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-bottom panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-bottom']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

</div>
