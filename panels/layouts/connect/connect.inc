<?php

$plugin = array(
  'title' => t('Connect'),
  'category' => t('Landing Pages'),
  'icon' => 'icon.png',
  'css' => '../../../lib/grid.css',
  'theme' => 'connect',
  'regions' => array(
    'full-top' => t('Full Top'),
    'tencol-left' => t('Ten Column Left'),
    'sixcol-right' => t('Six Column Right'),
    'full-middle' => t('Full Middle'),
    'fourcol-left' => t('Four Column Left'),
    'fourcol-leftcenter' => t('Four Column Left Center'),
    'fourcol-rightcenter' => t('Four Column Right Center'),
    'fourcol-right' => t('Four Column Right'),
    'full-bottom' => t('Full Bottom')
  ),
);