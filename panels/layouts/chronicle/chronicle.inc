<?php

$plugin = array(
  'title' => t('Chronicle'),
  'category' => t('Landing Pages'),
  'icon' => 'icon.png',
  'css' => '../../../lib/grid.css',
  'theme' => 'chronicle',
  'regions' => array(
    'full-top' => t('Full Top'),
    'twelvecol-top-left' => t('Twelve Column Top Left'),
    'fourcol-top-right' => t('Four Column Top Right'),
    'eightcol-middle-left' => t('Eight Column Middle Left'),
    'fourcol-middle-center' => t('Four Column Middle Center'),
    'fourcol-middle-right' => t('Four Column Middle Right'),
    'fourcol-bottom-left' => t('Four Column Bottom Left'),
    'fourcol-bottom-leftcenter' => t('Four Column Bottom Left Center'),
    'fourcol-bottom-rightcenter' => t('Four Column Bottom Right Center'),
    'fourcol-bottom-right' => t('Four Column Bottom Right'),
    'full-bottom' => t('Full Bottom')
  ),
);