<div class="panel-display chronicle container_16" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <?php if (!empty($content['full-top'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-top panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-top']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['twelvecol-top-left']) || !empty($content['fourcol-top-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-12-4 clearfix">
    <div class="panel-twelvecol-top-left panel-panel panel-col panel-col-first grid_12">
      <div class="panel-content-color">
        <?php print $content['twelvecol-top-left']; ?>
      </div>
    </div>
    <div class="panel-fourcol-top-right panel-panel panel-col panel-col-last grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-top-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['eightcol-middle-left']) || !empty($content['fourcol-middle-center']) || !empty($content['fourcol-middle-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-8-4-4 clearfix">
    <div class="panel-eightcol-middle-left panel-panel panel-col panel-col-first grid_8">
      <div class="panel-content-color">
        <?php print $content['eightcol-middle-left']; ?>
      </div>
    </div>
    <div class="panel-fourcol-middle-center panel-panel panel-col grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-middle-center']; ?>
      </div>
    </div>
    <div class="panel-fourcol-middle-right panel-panel panel-col panel-col-last grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-middle-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['fourcol-bottom-left']) || !empty($content['fourcol-bottom-leftcenter']) || !empty($content['fourcol-bottom-rightcenter']) || !empty($content['fourcol-bottom-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-4-4-4-4 clearfix">
    <div class="panel-fourcol-bottom-left panel-panel panel-col panel-col-first grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-bottom-left']; ?>
      </div>
    </div>
    <div class="panel-fourcol-bottom-leftcenter panel-panel panel-col grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-bottom-leftcenter']; ?>
      </div>
    </div>
    <div class="panel-fourcol-bottom-rightcenter panel-panel panel-col grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-bottom-rightcenter']; ?>
      </div>
    </div>
    <div class="panel-fourcol-bottom-right panel-panel panel-col panel-col-last grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-bottom-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['full-bottom'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-bottom panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-bottom']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

</div>
