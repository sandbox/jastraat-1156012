<?php

$plugin = array(
  'title' => t('Innovate'),
  'category' => t('Landing Pages'),
  'icon' => 'icon.png',
  'css' => '../../../lib/grid.css',
  'theme' => 'innovate',
  'regions' => array(
    'full-top' => t('Full Top'),
    'sixcol-top-left' => t('Six Column Top Left'),
    'tencol-top-right' => t('Ten Column Top Right'),
    'full-middle' => t('Full Middle'),
    'tencol-middle-left' => t('Ten Column Middle Left'),
    'sixcol-middle-right' => t('Six Column Middle Right'),
    'sixcol-bottom-left' => t('Six Column Bottom Left'),
    'tencol-bottom-right' => t('Ten Column Bottom Right'),
    'full-bottom' => t('Full Bottom')
  ),
);