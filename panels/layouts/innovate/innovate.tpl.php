<div class="panel-display innovate container_16" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <?php if (!empty($content['full-top'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-top panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-top']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['sixcol-top-left']) || !empty($content['tencol-top-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-6-10-first clearfix">
    <div class="panel-sixcol-top-left panel-panel panel-col panel-col-first grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-top-left']; ?>
      </div>
    </div>
    <div class="panel-tencol-top-right panel-panel panel-col panel-col-last grid_10">
      <div class="panel-content-color">
        <?php print $content['tencol-top-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['full-middle'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-middle panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-middle']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  
  <?php if (!empty($content['tencol-middle-left']) || !empty($content['sixcol-middle-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-10-6 clearfix">
    <div class="panel-tencol-middle-left panel-panel panel-col panel-col-first grid_10">
      <div class="panel-content-color">
        <?php print $content['tencol-middle-left']; ?>
      </div>
    </div>
    <div class="panel-sixcol-middle-right panel-panel panel-col panel-col-last grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-middle-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  
  <?php if (!empty($content['sixcol-bottom-left']) || !empty($content['tencol-bottom-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-6-10-second clearfix">
    <div class="panel-sixcol-bottom-left panel-panel panel-col panel-col-first grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-bottom-left']; ?>
      </div>
    </div>
    <div class="panel-tencol-bottom-right panel-panel panel-col panel-col-last grid_10">
      <div class="panel-content-color">
        <?php print $content['tencol-bottom-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['full-bottom'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-bottom panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-bottom']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

</div>
