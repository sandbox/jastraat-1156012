<div class="panel-display left-sidebar" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
   <?php if (!empty($content['leftSb'])){ ?>
    <div class="panel-row panel-row-4-12 container_16 clearfix">
      <div class="panel-col-last grid_12">
        <div class="container_12">

  <?php } else { ?>
    <div class="container_12 clearfix">
  <?php } ?>

    <?php if (!empty($content['top'])): ?>
        <div class="panel-row clearfix">
          <div class="panel-top panel-panel panel-col-full grid_12">
            <div class="panel-content-color">
              <?php print $content['top']; ?>
            </div>
          </div>
        </div>
    <?php endif; ?>

      <?php if (!empty($content['sevencol-left']) || !empty($content['fivecol-right'])): ?>
        <div class="panel-row panel-row-resize panel-row-7-5 clearfix">
          <div class="panel-sevencol-left panel-panel panel-col panel-col-first grid_7">
            <div class="panel-content-color">
              <?php print $content['sevencol-left']; ?>
            </div>
          </div>
          <div class="panel-fivecol-right panel-panel panel-col panel-col-last grid_5">
            <div class="panel-content-color">
              <?php print $content['fivecol-right']; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <?php if (!empty($content['eightcol-top-left']) || !empty($content['fourcol-top-right'])): ?>
        <div class="panel-row panel-row-resize panel-row-8-4 clearfix">
          <div class="panel-eightcol-top-left panel-panel panel-col panel-col-first grid_8">
            <div class="panel-content-color">
              <?php print $content['eightcol-top-left']; ?>
            </div>
          </div>
          <div class="panel-fourcol-top-right panel-panel panel-col panel-col-last grid_4">
            <div class="panel-content-color">
              <?php print $content['fourcol-top-right']; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <?php if (!empty($content['middle'])): ?>
        <div class="panel-row clearfix">
          <div class="panel-middle panel-panel panel-col-full grid_12">
            <div class="panel-content-color">
              <?php print $content['middle']; ?>
            </div>
           </div>
        </div>
      <?php endif; ?>

      <?php if (!empty($content['fourcol-bottom-left']) || !empty($content['eightcol-bottom-right'])): ?>
        <div class="panel-row panel-row-resize panel-row-4-8 clearfix">
          <div class="panel-fourcol-bottom-left panel-panel panel-col panel-col-first grid_4">
            <div class="panel-content-color">
              <?php print $content['fourcol-bottom-left']; ?>
            </div>
          </div>
          <div class="panel-eightcol-bottom-right panel-panel panel-col panel-col-last grid_8">
            <div class="panel-content-color">
              <?php print $content['eightcol-bottom-right']; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <?php if (!empty($content['sixcol-left']) || !empty($content['sixcol-right'])): ?>
        <div class="panel-row panel-row-resize panel-row-6-6 clearfix">
          <div class="panel-sixcol-left panel-panel panel-col panel-col-first grid_6">
            <div class="panel-content-color">
              <?php print $content['sixcol-left']; ?>
            </div>
          </div>
          <div class="panel-sixcol-right panel-panel panel-col panel-col-last grid_6">
            <div class="panel-content-color">
              <?php print $content['sixcol-right']; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <?php if (!empty($content['bottom'])): ?>
        <div class="panel-row clearfix">
          <div class="panel-bottom panel-panel panel-col-full grid_12">
            <div class="panel-content-color">
              <?php print $content['bottom']; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

  <?php if (!empty($content['leftSb'])){ ?>
      </div></div>
      <div class="panel-leftSb panel-panel panel-col-first grid_4">
        <div class="panel-content-color">
          <?php print $content['leftSb']; ?>
        </div>
      </div>
  <?php } ?>
  </div>
</div>
