<?php

$plugin = array(
  'title' => t('Left Sidebar'),
  'category' => t('Node Layouts'),
  'icon' => 'icon.png',
  'css' => '../../../lib/grid.css',
  'theme' => 'left_sidebar',
  'regions' => array(
    'leftSb' => t('Left Sidebar'),
    'top' => t('Top'),
	'sevencol-left' => t('Seven Column Left'),
    'fivecol-right' => t('Five Column Right'),
	'eightcol-top-left' => t('Eight Column Top Left'),
    'fourcol-top-right' => t('Four Column Top Right'),
	'middle' => t('Middle'),
	'fourcol-bottom-left' => t('Four Column Bottom Left'),
    'eightcol-bottom-right' => t('Eight Column Bottom Right'),
    'sixcol-left' => t('Six Column Left'),
    'sixcol-right' => t('Six Column Right'),
    'bottom' => t('Bottom')
  ),
);
