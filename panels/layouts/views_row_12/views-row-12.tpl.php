<div class="panel-display views_row_12" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="container_12 clearfix">

    <?php if (!empty($content['top'])): ?>
        <div class="panel-row clearfix">
          <div class="panel-top panel-panel panel-col-full grid_12">
            <div class="panel-content-color">
              <?php print $content['top']; ?>
            </div>
          </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($content['fourcol-left']) || !empty($content['fourcol-center'])|| !empty($content['fourcol-right'])): ?>
      <div class="panel-row panel-row-4-4-4 clearfix">
        <div class="panel-fourcol-left panel-panel panel-col panel-col-first grid_4">
          <div class="panel-content-color">
            <?php print $content['fourcol-left']; ?>
          </div>
        </div>
        <div class="panel-fourcol-center panel-panel panel-col grid_4">
          <div class="panel-content-color">
            <?php print $content['fourcol-center']; ?>
          </div>
        </div>
        <div class="panel-fourcol-right panel-panel panel-col panel-col-last grid_4">
          <div class="panel-content-color">
            <?php print $content['fourcol-right']; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if (!empty($content['onecol-top-left']) || !empty($content['fivecol-top-center']) || !empty($content['sixcol-top-right'])): ?>
      <div class="panel-row panel-row-1-5-6 clearfix">
        <div class="panel-onecol-top-left panel-panel panel-col panel-col-first grid_1">
          <div class="panel-content-color">
            <?php print $content['onecol-top-left']; ?>
          </div>
        </div>
        <div class="panel-fivecol-top-center panel-panel panel-col grid_5">
         <div class="panel-content-color">
            <?php print $content['fivecol-top-center']; ?>
         </div>
        </div>
        <div class="panel-sixcol-top-right panel-panel panel-col panel-col-last grid_6">
          <div class="panel-content-color">
            <?php print $content['sixcol-top-right']; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if (!empty($content['twocol-middle-left']) || !empty($content['fivecol-middle-center']) || !empty($content['fivecol-middle-right'])): ?>
      <div class="panel-row panel-row-2-5-5 clearfix">
        <div class="panel-twocol-middle-left panel-panel panel-col panel-col-first grid_2">
          <div class="panel-content-color">
            <?php print $content['twocol-middle-left']; ?>
          </div>
        </div>
        <div class="panel-fivecol-middle-center panel-panel panel-col grid_5">
         <div class="panel-content-color">
            <?php print $content['fivecol-middle-center']; ?>
         </div>
        </div>
        <div class="panel-fivecol-middle-right panel-panel panel-col panel-col-last grid_5">
          <div class="panel-content-color">
            <?php print $content['fivecol-middle-right']; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if (!empty($content['fivecol-bottom-left']) || !empty($content['sevencol-bottom-right'])): ?>
      <div class="panel-row panel-row-5-7 clearfix">
        <div class="panel-fivecol-bottom-left panel-panel panel-col panel-col-first grid_5">
          <div class="panel-content-color">
            <?php print $content['fivecol-bottom-left']; ?>
          </div>
        </div>
        <div class="panel-sevencol-bottom-right panel-panel panel-col panel-col-last grid_7">
          <div class="panel-content-color">
            <?php print $content['sevencol-bottom-right']; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if (!empty($content['sixcol-left']) || !empty($content['sixcol-right'])): ?>
      <div class="panel-row panel-row-6-6 clearfix">
        <div class="panel-sixcol-left panel-panel panel-col panel-col-first grid_6">
          <div class="panel-content-color">
            <?php print $content['sixcol-left']; ?>
          </div>
        </div>
        <div class="panel-sixcol-right panel-panel panel-col panel-col-last grid_6">
          <div class="panel-content-color">
            <?php print $content['sixcol-right']; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    
    <?php if (!empty($content['threecol-bottom-left']) || !empty($content['twocol-bottom-left-center']) || !empty($content['threecol-bottom-right-center']) || !empty($content['fourcol-bottom-right'])): ?>
      <div class="panel-row panel-row-3-2-3-4 clearfix">
        <div class="panel-threecol-bottom-left panel-panel panel-col panel-col-first grid_3">
          <div class="panel-content-color">
            <?php print $content['threecol-bottom-left']; ?>
          </div>
        </div>
        <div class="panel-twocol-bottom-left-center panel-panel panel-col grid_2">
         <div class="panel-content-color">
            <?php print $content['twocol-bottom-left-center']; ?>
         </div>
        </div>
        <div class="panel-threecol-bottom-right-center panel-panel panel-col grid_3">
         <div class="panel-content-color">
            <?php print $content['threecol-bottom-right-center']; ?>
         </div>
        </div>
        <div class="panel-fourcol-bottom-right panel-panel panel-col panel-col-last grid_4">
          <div class="panel-content-color">
            <?php print $content['fourcol-bottom-right']; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if (!empty($content['bottom'])): ?>
      <div class="panel-row clearfix">
        <div class="panel-bottom panel-panel panel-col-full grid_12">
          <div class="panel-content-color">
            <?php print $content['bottom']; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

  </div>
</div>
