<?php

$plugin = array(
  'title' => t('Views row: 12'),
  'category' => t('Views Layouts'),
  'icon' => 'icon.png',
  'css' => '../../../lib/grid.css',
  'theme' => 'views_row_12',
  'regions' => array(
    'top' => t('Top'),
    'fourcol-left' => t('Four Column Left'),
    'fourcol-center' => t('Four Column Center'),
    'fourcol-right' => t('Four Column Right'),
    'onecol-top-left' => t('One Column Top Left'),
    'fivecol-top-center' => t('Five Column Top Center'),
    'sixcol-top-right' => t('Six Column Top Right'),
    'twocol-middle-left' => t('Two Column Middle Left'),
    'fivecol-middle-center' => t('Five Column Middle Center'),
    'fivecol-middle-right' => t('Five Column Middle Right'),
    'fivecol-bottom-left' => t('Five Column Bottom Left'),
    'sevencol-bottom-right' => t('Seven Column Bottom Right'),
    'sixcol-left' => t('Six Column Left'),
    'sixcol-right' => t('Six Column Right'),
    'threecol-bottom-left' => t('Three Column Bottom Left'),
    'twocol-bottom-left-center' => t('Two Column Bottom Left Center'),
    'threecol-bottom-right-center' => t('Three Column Bottom Right Center'),
    'fourcol-bottom-right' => t('Four Column Bottom Right'),
    'bottom' => t('Bottom')
  ),
);
