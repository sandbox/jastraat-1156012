<?php

$plugin = array(
  'title' => t('Captivate'),
  'category' => t('Landing Pages'),
  'icon' => 'icon.png',
  'css' => '../../../lib/grid.css',
  'theme' => 'captivate',
  'regions' => array(
    'full-top' => t('Full Top'),
    'elevencol-top-left' => t('Eleven Column Top Left'),
    'fivecol-top-right' => t('Five Column Top Right'),
    'fivecol-middle-left' => t('Five Column Middle Left'),
    'sixcol-middle-center' => t('Six Column Middle Center'),
    'fivecol-middle-right' => t('Five Column Middle Right'),
    'elevencol-bottom-left' => t('Eleven Column Bottom Left'),
    'fivecol-bottom-right' => t('Five Column Bottom Right'),
    'full-bottom' => t('Full Bottom')
  ),
);