<div class="panel-display captivate container_16" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <?php if (!empty($content['full-top'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-top panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-top']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['elevencol-top-left']) || !empty($content['fivecol-top-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-11-5 clearfix">
    <div class="panel-elevencol-top-left panel-panel panel-col panel-col-first grid_11">
      <div class="panel-content-color">
        <?php print $content['elevencol-top-left']; ?>
      </div>
    </div>
    <div class="panel-fivecol-top-right panel-panel panel-col panel-col-last grid_5">
      <div class="panel-content-color">
        <?php print $content['fivecol-top-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['fivecol-middle-left']) || !empty($content['sixcol-middle-center']) || !empty($content['fivecol-middle-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-5-6-5 clearfix">
    <div class="panel-fivecol-middle-left panel-panel panel-col panel-col-first grid_5">
      <div class="panel-content-color">
        <?php print $content['fivecol-middle-left']; ?>
      </div>
    </div>
    <div class="panel-sixcol-middle-center panel-panel panel-col grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-middle-center']; ?>
      </div>
    </div>
    <div class="panel-fivecol-middle-right panel-panel panel-col panel-col-last grid_5">
      <div class="panel-content-color">
        <?php print $content['fivecol-middle-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['elevencol-bottom-left']) || !empty($content['fivecol-bottom-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-11-5 clearfix">
    <div class="panel-elevencol-bottom-left panel-panel panel-col panel-col-first grid_11">
      <div class="panel-content-color">
        <?php print $content['elevencol-bottom-left']; ?>
      </div>
    </div>
    <div class="panel-fivecol-bottom-right panel-panel panel-col panel-col-last grid_5">
      <div class="panel-content-color">
        <?php print $content['fivecol-bottom-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  
  <?php if (!empty($content['full-bottom'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-bottom panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-bottom']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

</div>
