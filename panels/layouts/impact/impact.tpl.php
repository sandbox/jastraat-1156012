<div class="panel-display impact container_16" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <?php if (!empty($content['full-top'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-top panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-top']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['eightcol-left']) || !empty($content['eightcol-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-8-8 clearfix">
    <div class="panel-eightcol-left panel-panel panel-col panel-col-first grid_8">
      <div class="panel-content-color">
        <?php print $content['eightcol-left']; ?>
      </div>
    </div>
    <div class="panel-eightcol-right panel-panel panel-col panel-col-last grid_8">
      <div class="panel-content-color">
        <?php print $content['eightcol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['full-middle'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-middle panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-middle']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['sixcol-left']) || !empty($content['fourcol-center']) || !empty($content['sixcol-right'])): ?>
  <div class="panel-row panel-row-resize panel-row-6-4-6 clearfix">
    <div class="panel-sixcol-left panel-panel panel-col panel-col-first grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-left']; ?>
      </div>
    </div>
    <div class="panel-fourcol-center panel-panel panel-col grid_4">
      <div class="panel-content-color">
        <?php print $content['fourcol-center']; ?>
      </div>
    </div>
    <div class="panel-sixcol-right panel-panel panel-col panel-col-last grid_6">
      <div class="panel-content-color">
        <?php print $content['sixcol-right']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <?php if (!empty($content['full-bottom'])): ?>
  <div class="panel-row clearfix">
    <div class="panel-full-bottom panel-panel panel-col-full grid_16">
      <div class="panel-content-color">
        <?php print $content['full-bottom']; ?>
      </div>
    </div>
  </div>
  <?php endif; ?>

</div>
