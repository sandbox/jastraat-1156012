<?php

$plugin = array(
  'title' => t('Impact'),
  'category' => t('Landing Pages'),
  'icon' => 'icon.png',
  'css' => '../../../lib/grid.css',
  'theme' => 'impact',
  'regions' => array(
    'full-top' => t('Full Top'),
    'eightcol-left' => t('Eight Column Left'),
    'eightcol-right' => t('Eight Column Right'),
    'full-middle' => t('Full Middle'),
    'sixcol-left' => t('Six Column Left'),
    'fourcol-center' => t('Four Column Center'),
    'sixcol-right' => t('Six Column Right'),
    'full-bottom' => t('Full Bottom')
  ),
);