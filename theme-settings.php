<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function innovate_form_system_theme_settings_alter(&$form, $form_state) {
  $form['javascript_libraries'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Activate Libraries for IE'),
  );
  $form['javascript_libraries']['innovate_equalizejs'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Include JS to equalize column heights'),
    '#default_value' => theme_get_setting('innovate_equalizejs'),
    '#description'   => t('Sets all boxes within a panel row to the same height.'),
  );
  $form['javascript_libraries']['innovate_imagecaptions'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Include JS for automatic captions on inline images'),
    '#default_value' => theme_get_setting('innovate_imagecaptions'),
    '#description'   => t('Inline images within textareas are wrapped with a container div and the alt text is displayed as a caption below the image'),
  );
  $form['javascript_libraries']['innovate_css3pie'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Include CSS3 PIE'),
    '#default_value' => theme_get_setting('innovate_css3pie'),
    '#description'   => t('Allows the use of CSS3 styles in IE 7 and 8. (!url) Requires manual download. Place in !themepath/scripts. (Assumes PIE.htc file.) You will also need to uncomment the behavior line in ie.css and update the path if you are not using the sites/all/themes folder.',  array('!url' => l('CSS3 PIE', 'http://css3pie.com/'),  '!themepath' => drupal_get_path('theme', 'innovate'))),
  );
  $form['meta_tags'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Meta tags'),
  );
  $form['meta_tags']['innovate_meta_description'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use site slogan for meta description on home page'),
    '#default_value' => theme_get_setting('innovate_meta_description'),
    '#description'   => t('The meta description is displayed as the teaser in Google search results.'),
  );
  $form['meta_tags']['innovate_include_chrome_frame'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Include Chrome Frame for IE users with Chrome Frame installed'),
    '#default_value' => theme_get_setting('innovate_include_chrome_frame'),
    '#description'   => t('Include a meta tag to initialize Chrome Frame. Only affects IE users who already have Chrome Frame installed.'),
  );
  $form['meta_tags']['innovate_use_mobile_viewport'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use a mobile viewport meta tag'),
    '#default_value' => theme_get_setting('innovate_use_mobile_viewport'),
  );

  $form['meta_tags']['viewport_options'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        ':input[name="innovate_use_mobile_viewport"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['meta_tags']['viewport_options']['innovate_mobile_viewport'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Mobile viewport meta tag value'),
    '#default_value' => theme_get_setting('innovate_mobile_viewport'),
    '#description'   => t('device-width : Occupy full width of the screen in its current orientation<br/>
                          initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height<br/>
                          maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width<br/>
                          suggested default: width=device-width; initial-scale=1.0; maximum-scale=1.0;
                          '),
  );

  $form['layout'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Layout settings'),
  );
  $form['layout']['innovate_no_pane_margins'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Do not include margins between panel panes within this theme'),
    '#default_value' => theme_get_setting('innovate_no_pane_margins'),
  );
  $form['layout']['innovate_use_fixed_layout'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use a fixed width design'),
    '#default_value' => theme_get_setting('innovate_use_fixed_layout'),
    '#description'   => t('This will prevent mobile viewport flexibility for devices like the iPad (although the single column layout will still work for smaller mobile devices). Note: IE 8 and below will be fixed width regardless.'),
  );
  $form['breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb settings'),
  );
  $form['breadcrumb']['innovate_breadcrumb'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('innovate_breadcrumb'),
    '#options'       => array(
      'yes'   => t('Yes'),
      'admin' => t('Only in admin section'),
      'no'    => t('No'),
    ),
  );
  $form['breadcrumb']['breadcrumb_options'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        ':input[name="innovate_breadcrumb"]' => array('value' => 'no'),
      ),
    ),
  );
  $form['breadcrumb']['breadcrumb_options']['innovate_breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Do not forget to include spaces.'),
    '#default_value' => theme_get_setting('innovate_breadcrumb_separator'),
    '#size'          => 5,
    '#maxlength'     => 10,
  );
  $form['breadcrumb']['breadcrumb_options']['innovate_breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show home page link in breadcrumb'),
    '#default_value' => theme_get_setting('innovate_breadcrumb_home'),
  );
  $form['breadcrumb']['breadcrumb_options']['innovate_breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('innovate_breadcrumb_title'),
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
  );
  $form['breadcrumb']['breadcrumb_options']['innovate_breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('innovate_breadcrumb_trailing'),
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
    '#states' => array(
      'disabled' => array(
        ':input[name="innovate_breadcrumb_title"]' => array('checked' => TRUE),
      ),
      'unchecked' => array(
        ':input[name="innovate_breadcrumb_title"]' => array('checked' => FALSE),
      ),
    ),
  );
}
